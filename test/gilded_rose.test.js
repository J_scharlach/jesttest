const {Shop, Item} = require("../src/gilded_rose");

describe("Gilded Rose", function() {
  it("should foo", function() {
    const gildedRose = new Shop([new Item("foo", 0, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("foo");
  });
});



test("common items (this time Dex) the quality should never be below 0", () => {
  const storeItems = [new Item("+5 Dexterity Vest", 10, 0)];
  const expectedResult = [new Item("+5 Dexterity Vest", 9, 0)];
  const gildedRose = new Shop(storeItems);
  const items = gildedRose.updateQuality();

  expect(items).toStrictEqual(expectedResult);
});

test("common items (this time Elix) with each passing day, quality should be reduced by a factor of 1", () => {
  const storeItems = [new Item("Elixir of the Mongoose", 5, 7)];
  const expectedResult = [new Item("Elixir of the Mongoose", 4, 6)];
  const gildedRose = new Shop(storeItems);
  const items = gildedRose.updateQuality();

  expect(items).toStrictEqual(expectedResult);
});

test("Aged Brie the quality should never go above 50", () => {
  const storeItems = [new Item("Aged Brie", 1, 50)];
  const expectedResult = [new Item("Aged Brie", 0, 50)];
  const gildedRose = new Shop(storeItems);
  const items = gildedRose.updateQuality();

  expect(items).toStrictEqual(expectedResult);
});

test("quality of Aged Brie should increase by 1", () => {
  const storeItems = [new Item("Aged Brie", 1, 0)];
  const expectedResult = [new Item("Aged Brie", 0, 1)];
  const gildedRose = new Shop(storeItems);
  const items = gildedRose.updateQuality();

  expect(items).toStrictEqual(expectedResult);
});

test("Conjured Mana Cake with each passing day, quality should be reduced by a factor of 2", () => {
  const storeItems = [new Item("Conjured Mana Cake", 3, 6)];
  const expectedResult = [new Item("Conjured Mana Cake", 2, 4)];
  const gildedRose = new Shop(storeItems);
  const items = gildedRose.updateQuality();

  expect(items).toStrictEqual(expectedResult);
});

describe("Backstage passes", () => {
  test("quality increases as sellIn date gets closer", () => {
    const storeItems = [
      new Item("Backstage passes to a TAFKAL80ETC concert", 14, 0)
    ];
    const expectedResult = [
      new Item("Backstage passes to a TAFKAL80ETC concert", 13, 1)
    ];
    const gildedRose = new Shop(storeItems);
    const items = gildedRose.updateQuality();

    expect(items).toStrictEqual(expectedResult);
  });

  test("when the sellIn date gets to <10, the quality should go up by 2 with each passing day", () => {
    const storeItems = [
      new Item("Backstage passes to a TAFKAL80ETC concert", 10, 0)
    ];
    const expectedResult = [
      new Item("Backstage passes to a TAFKAL80ETC concert", 9, 2)
    ];
    const gildedRose = new Shop(storeItems);
    const items = gildedRose.updateQuality();

    expect(items).toStrictEqual(expectedResult);
  });

  test("when the sellIn date gets to < 5, the quality should go up by 3 with each passing day", () => {
    const storeItems = [
      new Item("Backstage passes to a TAFKAL80ETC concert", 5, 0)
    ];
    const expectedResult = [
      new Item("Backstage passes to a TAFKAL80ETC concert", 4, 3)
    ];
    const gildedRose = new Shop(storeItems);
    const items = gildedRose.updateQuality();

    expect(items).toStrictEqual(expectedResult);
  });

  test("finally the quality drops to 0 when the sellIn date has passed", () => {
    const storeItems = [
      new Item("Backstage passes to a TAFKAL80ETC concert", 0, 30)
    ];
    const expectedResult = [
      new Item("Backstage passes to a TAFKAL80ETC concert", -1, 0)
    ];
    const gildedRose = new Shop(storeItems);
    const items = gildedRose.updateQuality();

    expect(items).toStrictEqual(expectedResult);
  });
});